package com.proxiel.miniproject;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.proxiel.miniproject.domain.User;
import com.proxiel.miniproject.repository.UserRepository;
import com.proxiel.miniproject.services.UserService;

@SpringBootTest

class MiniProjectApplicationTests {

    @Test
    void contextLoads() {
    }
    
    @Autowired
    private UserService userService;
    
    @MockBean
    private UserRepository userRepository;
    
    @Before
    public void setUp() {
    	Optional<User> user =  Optional.of(new User(1L, "oussama","bs.ouss@gamil.com","ouss", "bs","Cergy","France",25,"0000"));
        Mockito.when(userRepository.findById(1L))
          .thenReturn(user);
        
        Optional<User> user2 =  null;
        Mockito.when(userRepository.findById(2L))
          .thenReturn(user2);
    }

}
