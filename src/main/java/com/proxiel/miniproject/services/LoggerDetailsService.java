package com.proxiel.miniproject.services;



import com.proxiel.miniproject.domain.LoggerDetails;
import com.proxiel.miniproject.repository.LoggerDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@Transactional
public class LoggerDetailsService {

    @Autowired
    private LoggerDetailsRepository loggerDetailsRepository;

    /**
     * log the input and output of each call and the processing time.
     * @param message
     * @param detaills
     */

    public  void logMethodeUser(String message , String detaills  ){
        LoggerDetails log = new LoggerDetails();
        log.setMessage( message );
        log.setTimestamp(  LocalDate.now() );
        log.setDetails( detaills );
        loggerDetailsRepository.save(log);
    }
}
