package com.proxiel.miniproject.services;

import com.proxiel.miniproject.domain.User;
import com.proxiel.miniproject.repository.UserRepository;
import com.proxiel.miniproject.util.UserStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    LoggerDetailsService loggerDetailsService;

    /**
     *  Service : Allows to register a user
     * @param user
     * @return
     */
    public Long addUser(User user) {
        Optional<User> userExist = userRepository.findByusername(user.getUsername());
        if(user.getUsername() != null && user.getEmail() != null  && user.getAge()!= 0 && user.getCountry()!= null && user.getPassword()!=null && !userExist.isPresent())  {
    	    if ( (user.getAge() > 18) && user.getCountry().equals( "France")) {
            	userRepository.save(user);
                loggerDetailsService.logMethodeUser("User Added",HttpStatus.OK.toString());
            	return UserStatus.OK_FOR_REGISTRATION.getCode();
    	    } else{
                  loggerDetailsService.logMethodeUser("only adults  and that live in France can create an account!",HttpStatus.UNAUTHORIZED.toString());
                     return  UserStatus.UNAUTHORIZED.getCode();
    	    }
        }else {
            System.out.println(UserStatus.REQUIRED_FIELD.getCode());
            loggerDetailsService.logMethodeUser("Required field",HttpStatus.NOT_ACCEPTABLE.toString());
            return  UserStatus.REQUIRED_FIELD.getCode();
        }
    }

    /**
     *   Service: Allows to displays the details of a registered user.
     * @param id
     * @return
     *
     */
    public User getUserById(Long id) {
    	Optional<User> userExist = userRepository.findById(id);
        if(userExist.isPresent()){
            loggerDetailsService.logMethodeUser("Find user with id " + id,HttpStatus.OK.toString());
        } else{
            loggerDetailsService.logMethodeUser("no user with provided id.",HttpStatus.NOT_FOUND.toString());
        }
        return userExist.isPresent() ? userExist.get() : null;
    }
    /**
     *  Service : Allows to login a user
     * @param username
     * @param password
     * @return
     */
    public User checkLogin(String username, String password){
        Optional<User> userExist = userRepository.findByusernameAndpassword(username,password);
        if(userExist.isPresent()){
            loggerDetailsService.logMethodeUser("Find user with username " + username + "and password " + password,HttpStatus.OK.toString());
        } else{
            loggerDetailsService.logMethodeUser("no user with provided username and password.",HttpStatus.NOT_FOUND.toString());
        }
        return userExist.isPresent() ? userExist.get() : null;
    }

}
