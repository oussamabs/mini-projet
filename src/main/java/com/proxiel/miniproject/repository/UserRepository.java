package com.proxiel.miniproject.repository;

import com.proxiel.miniproject.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.jws.soap.SOAPBinding;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT user from User user where user.username = :login and user.password = :password")
    Optional<User> findByusernameAndpassword(@Param("login") String username, @Param("password") String password);

    @Query("SELECT user from User user where user.username = :username")
    Optional<User> findByusername(@Param("username") String username);
}
