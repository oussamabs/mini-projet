package com.proxiel.miniproject.repository;

import com.proxiel.miniproject.domain.LoggerDetails;
import com.proxiel.miniproject.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoggerDetailsRepository extends JpaRepository<LoggerDetails, Long> {
}
