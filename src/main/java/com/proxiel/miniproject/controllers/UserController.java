package com.proxiel.miniproject.controllers;

import com.proxiel.miniproject.domain.LoggerDetails;
import com.proxiel.miniproject.domain.User;
import com.proxiel.miniproject.repository.UserRepository;
import com.proxiel.miniproject.services.UserService;
import com.proxiel.miniproject.util.UserStatus;

import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
//import javax.validation.Valid;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/")
@Aspect
public class UserController {

    @Autowired
     private UserService userService;

     Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     *   Controller: Allows to displays the details of a registered user.
     * @param id
     * @return
     */

    @GetMapping("user/{id}")
    public ResponseEntity findById(@PathVariable Long id) {
        logger.debug("request to get user having id :{}" + id);

        User user  =  userService.getUserById(id);
        if ( user != null ){
            return new ResponseEntity( user, HttpStatus.OK);
        }else {
            return new ResponseEntity( "no user with provided id.", HttpStatus.NOT_FOUND);
        }
    }

    /**
     *  Controller: Allows to register a user
     * @param user
     * @return
     */
    @PostMapping("user/add")
    public ResponseEntity addUser( @Valid @RequestBody User user) {
         logger.debug("request to add User ");
         Long statusUser = userService.addUser(user);
        if (statusUser.equals( UserStatus.OK_FOR_REGISTRATION.getCode()) ) {
            return new ResponseEntity("User Added",HttpStatus.OK);
        } else if (statusUser == UserStatus.UNAUTHORIZED.getCode()) {
            return new ResponseEntity("only adults  and that live in France can create an account!", HttpStatus.UNAUTHORIZED);
        } else if (statusUser == UserStatus.REQUIRED_FIELD.getCode()) {
            return new ResponseEntity("Required Filds!", HttpStatus.NOT_ACCEPTABLE);
        } else {
            return new ResponseEntity("Error!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     *   Controller: Allows to displays the details of a logged user.
     * @param username
     * @param password
     * @return
     */
    @GetMapping("user/login/{username}/{password}")
    public ResponseEntity checkLogin(@PathVariable("username") String username, @PathVariable("password") String password ) {
        logger.debug("request to get user having username and password :{}" + username + ":" + password );

        User user  =  userService.checkLogin(username, password);
        if ( user != null ){
            return new ResponseEntity( user, HttpStatus.OK);
        }else {
            return new ResponseEntity( "no user with provided username and password.", HttpStatus.NOT_FOUND);
        }
    }

}





