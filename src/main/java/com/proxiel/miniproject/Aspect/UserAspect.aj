package com.proxiel.miniproject.Aspect;

import com.proxiel.miniproject.domain.LoggerDetails;
import com.proxiel.miniproject.repository.LoggerDetailsRepository;
import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

public aspect UserAspect {
    @Autowired
    private LoggerDetailsRepository loggerDetailsRepository;

    pointcut PC1():execution(* com.proxiel.miniproject.controllers.UserController.findById(..));

    before():PC1 (){
      System.out.println("Before find details of a registered user  from  Aspectj syntax ");
    }

    after():PC1 (){
        System.out.println("After details of a registered user  from  Aspectj syntax ");
        //  LoggerDetails log = new LoggerDetails();
        //log.setMessage("Find details of a registered user");
        //log.setTimestamp(  LocalDate.now());
       // log.setDetails("");
       // loggerDetailsRepository.save(log);
    }

    pointcut PC2():execution(* com.proxiel.miniproject.controllers.UserController.addUser(..));
    before():PC2 (){
        System.out.println("Before add  from  Aspectj syntax ");
    }

    after():PC2 (){
        System.out.println("After add  from  Aspectj syntax ");
       // LoggerDetails log = new LoggerDetails();
         // log.setMessage("add user");
         // log.setTimestamp(  LocalDate.now());
        //log.setDetails("");
        // System.out.println("testetst"+ jp);
       // loggerDetailsRepository.save(log);
    }
}
